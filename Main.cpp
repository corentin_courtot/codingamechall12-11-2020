#pragma GCC optimize("Ofast")

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <chrono>
#include <thread>

class Ingredients
{
private:
    int ingredients[4];

public:
    Ingredients(int delta0 = 0, int delta1 = 0, int delta2 = 0, int delta3 = 0)
    {
        this->ingredients[0] = delta0;
        this->ingredients[1] = delta1;
        this->ingredients[2] = delta2;
        this->ingredients[3] = delta3;
    }
    ~Ingredients() {}

    bool is_positive() { return ingredients[0] >= 0 && ingredients[1] >= 0 && ingredients[2] >= 0 && ingredients[3] >= 0; }
    bool is_not_over_10() { return ingredients[0] + ingredients[1] + ingredients[2] + ingredients[3] <= 10; }
    void set(int delta0 = 0, int delta1 = 0, int delta2 = 0, int delta3 = 0)
    {
        this->ingredients[0] = delta0;
        this->ingredients[1] = delta1;
        this->ingredients[2] = delta2;
        this->ingredients[3] = delta3;
    }
    int get_score() { return ingredients[1] + ingredients[2] + ingredients[3]; }
    float get_fitness() { return ingredients[0] * 1 + ingredients[1] * 2 + ingredients[2] * 3 + ingredients[3] * 4; }

    friend void operator+=(Ingredients &i1, Ingredients const &i2)
    {
        i1.ingredients[0] += i2.ingredients[0];
        i1.ingredients[1] += i2.ingredients[1];
        i1.ingredients[2] += i2.ingredients[2];
        i1.ingredients[3] += i2.ingredients[3];
    }
    friend Ingredients operator*(const Ingredients &i, const int &t)
    {
        return Ingredients(i.ingredients[0] * t, i.ingredients[1] * t, i.ingredients[2] * t, i.ingredients[3] * t);
    }
    friend Ingredients operator+(const Ingredients &i1, Ingredients const &i2)
    {
        return Ingredients(i1.ingredients[0] + i2.ingredients[0], i1.ingredients[1] + i2.ingredients[1], i1.ingredients[2] + i2.ingredients[2], i1.ingredients[3] + i2.ingredients[3]);
    }
    friend std::ostream &operator<<(std::ostream &os, Ingredients const &ing)
    {
        os << "ingredients bilan : [" << ing.ingredients[0] << ", " << ing.ingredients[1] << ", " << ing.ingredients[2] << ", " << ing.ingredients[3] << "]" << std::endl;
        return os;
    }
};

class Action
{
private:
    int id;
    std::string actionType;
    Ingredients ingredients;
    int price;
    bool castable;
    bool repeatable;

    bool isActual;

    float choosingScore; // value modifying the likelyhood for this action to be selected over another. Higher value means more likely to be chosen

public:
    Action(std::string actionType = "") : id(0), actionType("REST"), price(0), castable(true), isActual(true), choosingScore(1.f), repeatable(false) {}
    Action(int id, std::string actionType, int delta0, int delta1, int delta2, int delta3, int price = 0, bool castable = true, bool repeatable = false) : id(id), actionType(actionType), ingredients(delta0, delta1, delta2, delta3), price(price), castable(castable), repeatable(repeatable), isActual(true)
    {
        if (actionType == "CAST")
        {
            this->choosingScore = 1.f + this->ingredients.get_fitness();
            if (this->repeatable)
            {
                this->choosingScore += 1;
            }
        }
        else if (actionType == "BREW")
            this->choosingScore = 2 * price;
        else if (actionType == "LEARN")
            this->choosingScore = 0; // TO DO

        this->choosingScore *= this->choosingScore;
    }
    ~Action() {}

    int get_id() const { return this->id; }
    int get_price() const { return this->price; }
    Ingredients get_ingredients() const { return this->ingredients; }
    std::string get_type() const { return this->actionType; }
    bool is_actual() const { return this->isActual; }
    bool is_castable() const { return this->actionType != "CAST" || this->castable; }
    bool is_repeatable() const { return this->repeatable; }
    float get_choosing_score() const { return this->choosingScore; }

    void mark_as_old()
    {
        if (actionType != "REST")
            this->isActual = false;
    }
    void renew(int price, int tomeIndex, int taxCount, bool castable, bool repeatable)
    {
        this->isActual = true;
        this->price = price;
        this->castable = castable;
    }
    void use()
    {
        if (this->actionType == "CAST")
        {
            castable = false;
        }
    }
    void reload() { this->castable = true; }

    void display() const
    {
        std::cout << actionType;
        if (actionType != "REST")
            std::cout << " " << id;
        std::cout << std::endl;
    }

    friend std::ostream &operator<<(std::ostream &os, Action const &action)
    {
        os << "id : " << action.id << std::endl;
        os << "actionType : " << action.actionType << std::endl;
        os << action.ingredients;
        os << "price : " << action.price << std::endl;
        os << "castable : " << action.castable << std::endl;
        os << "is actual : " << action.isActual << std::endl;
        os << "choosing score : " << action.choosingScore << std::endl;
        return os;
    }
};

class State
{
private:
    std::vector<Action> actions;
    Ingredients ingredientsOwned;
    int score;
    int turnNumber;
    int nbPots;

public:
    State() : score(0), turnNumber(1), nbPots(0) { this->actions.resize(0); }
    ~State() {}

    int get_score() { return score + ingredientsOwned.get_score(); }
    float get_fitness() { return (get_score() + ingredientsOwned.get_fitness() * 0.1f) / turnNumber; }
    int get_turn() { return turnNumber; }
    bool is_end() { return turnNumber > 100 || nbPots >= 6; }
    Action *get_action_corresponding_to_id(const int &actionId)
    {
        for (std::vector<Action>::iterator it = this->actions.begin(); it != this->actions.end(); ++it)
        {
            if (it->get_id() == actionId)
                return &(*it);
        }
        return nullptr;
    }

    // init functions
    void addRestAction() { actions.push_back(Action()); }
    void addAction(int actionId, std::string actionType, int delta0, int delta1, int delta2, int delta3, int price, int tomeIndex, int taxCount, bool castable, bool repeatable)
    {
        if (actionType == "OPPONENT_CAST" || actionType == "LEARN")
            return;

        Action *actionPtr = this->get_action_corresponding_to_id(actionId);
        if (actionPtr != nullptr)
        {
            // action already exists
            actionPtr->renew(price, tomeIndex, taxCount, castable, repeatable);
            return;
        }
        actions.push_back(Action(actionId, actionType, delta0, delta1, delta2, delta3, price, castable, repeatable));
    }

    // prepare for next turn functions
    void prepareForNextTurn()
    {
        for (std::vector<Action>::iterator it = this->actions.begin(); it != this->actions.end(); ++it)
        {
            it->mark_as_old();
        }
    }
    void delete_old_actions()
    {
        for (std::vector<Action>::iterator it = this->actions.begin(); it != this->actions.end(); ++it)
        {
            if (!it->is_actual())
                this->actions.erase(it);
        }
    }

    void set(int inv0, int inv1, int inv2, int inv3, int score)
    {
        this->score = score;
        this->ingredientsOwned.set(inv0, inv1, inv2, inv3);
    }
    void use_action(const int &actionId)
    {
        Action *action = get_action_corresponding_to_id(actionId);
        this->turnNumber++;
        if (action->get_type() == "BREW")
        {
            this->score += action->get_price();
            this->ingredientsOwned += action->get_ingredients();
            action->use();
            this->nbPots++;
            delete_action(actionId);
        }
        else if (action->get_type() == "CAST")
        {
            this->ingredientsOwned += action->get_ingredients();
            action->use();
        }
        else if (action->get_type() == "LEARN")
        {
            this->ingredientsOwned += action->get_ingredients();
            action->use();
        }
        else if (action->get_type() == "REST")
        {
            for (auto it = this->actions.begin(); it != this->actions.end(); ++it)
            {
                it->reload();
            }
        }
    }
    void delete_action(const int &actionId)
    {
        for (std::vector<Action>::iterator it = this->actions.begin(); it != this->actions.end(); ++it)
        {
            if (it->get_id() == actionId)
            {
                this->actions.erase(it);
                return;
            }
        }
    }
    bool is_action_possible(Action *action, const int &times = 1)
    {
        if (action == nullptr)
            return false;
        Ingredients tempIngredients = this->ingredientsOwned + action->get_ingredients() * times;
        return tempIngredients.is_positive() && tempIngredients.is_not_over_10() && action->is_castable();
    }
    bool is_action_possible(const int &actionId)
    {
        return is_action_possible(get_action_corresponding_to_id(actionId));
    }
    void add_turn() { this->turnNumber++; }
    std::vector<Action *> get_possible_actions()
    {
        std::vector<Action *> possibleActions;
        for (auto it = actions.begin(); it < actions.end(); ++it)
        {
            if (is_action_possible(&(*it)))
            {
                possibleActions.push_back(&(*it));
            }
        }
        return possibleActions;
    }
    void copy(const State &state)
    {
        this->actions = state.actions;
        this->ingredientsOwned = state.ingredientsOwned;
        this->score = state.score;
        this->turnNumber = state.turnNumber;
        this->nbPots = state.nbPots;
    }

    friend std::ostream &operator<<(std::ostream &os, State const &state)
    {
        os << "score : " << state.score << std::endl;
        os << "turn : " << state.turnNumber << std::endl;
        os << state.ingredientsOwned;
        for (auto it = state.actions.begin(); it != state.actions.end(); ++it)
        {
            //os << "action :" << std::endl;
            //os << *it;
        }
        return os;
    }
};

class ActionIdAndTimesUsed
{
public:
    short actionId;
    short timesUsed;
    ActionIdAndTimesUsed(int actionId = -1, int timesUsed = -1) : actionId(actionId), timesUsed(timesUsed) {}
    ~ActionIdAndTimesUsed() {}
};

class Scenario
{
private:
    State finalState;
    State *initState;
    std::vector<ActionIdAndTimesUsed> actionsIdSequence;
    float fitness;

    // prepare for next turn functions
    void update_action_sequence()
    {
        auto it = this->actionsIdSequence.begin();
        while (it != this->actionsIdSequence.end())
        {
            if (!initState->get_action_corresponding_to_id((*it).actionId)->is_actual())
            {
                it = this->actionsIdSequence.erase(it);
            }
            else
            {
                ++it;
            }
        }
    }

    // simulation functions
    void init_simulation()
    {
        this->finalState.copy(*this->initState);
    }
    bool simulate_turn()
    {
        int nbTurnInFuture = finalState.get_turn() - initState->get_turn();
        while (!there_is_a_valid_action_planned(nbTurnInFuture))
        {
            if (actionsIdSequence.size() > nbTurnInFuture && !finalState.is_action_possible(actionsIdSequence[nbTurnInFuture].actionId))
            {
                // if there is an action planned but it is not valid, erase it
                actionsIdSequence.erase(actionsIdSequence.begin() + nbTurnInFuture);
            }
            if (actionsIdSequence.size() <= nbTurnInFuture)
            {
                // adding random action at the end
                if (!add_random_possible_action())
                {
                    std::cerr << "no action possible";
                    return false;
                }
            }
        }
        finalState.use_action(actionsIdSequence[nbTurnInFuture].actionId);

        return true;
    }
    bool there_is_a_valid_action_planned(const int &nbTurnInFuture)
    {
        return actionsIdSequence.size() > nbTurnInFuture && finalState.is_action_possible(actionsIdSequence[nbTurnInFuture].actionId);
    }
    bool add_random_possible_action()
    {
        std::vector<Action *> possibleActions = finalState.get_possible_actions();

        if (possibleActions.size() == 0)
            return false;

        int idChosen = -1;
        {
            // this block chooses cleverly an action among all the actions possible
            float totalActionsScore = 0.f;
            for (auto it = possibleActions.begin(); it != possibleActions.end(); it++)
            {
                totalActionsScore += (*it)->get_choosing_score();
            }

            float progress = 0.f;
            float randomValue = rand() * totalActionsScore / RAND_MAX;
            auto it = possibleActions.begin();
            while (progress < randomValue && it != possibleActions.end())
            {
                progress += (*it)->get_choosing_score();
                idChosen = (*it)->get_id();
                ++it;
            }
        }

        actionsIdSequence.push_back(ActionIdAndTimesUsed(idChosen, get_random_number_of_times_used(idChosen)));
        return true;
    }
    int get_random_number_of_times_used(const int &idChosen)
    {
        Action *actionChosen = finalState.get_action_corresponding_to_id(idChosen);
        if (actionChosen->is_repeatable())
        {
            int nbMax = 0;
            while (finalState.is_action_possible(actionChosen, nbMax + 1))
            {
                nbMax++;
            }
            return std::min((int)(1 + (long)rand() * 2 * nbMax / RAND_MAX), nbMax);
        }
        return 1;
    }

public:
    Scenario() : fitness(0.f)
    {
        this->actionsIdSequence.resize(0);
        this->initState = nullptr;
    }
    ~Scenario() {}

    float get_fitness() { return fitness; }
    void evaluate_fitness() { this->fitness = finalState.get_fitness(); }
    bool is_done() { return finalState.is_end(); }

    void copy(const Scenario &scenario)
    {
        this->initState = scenario.initState;
        this->finalState = scenario.finalState;
        this->actionsIdSequence.resize(0);
        for (auto it = scenario.actionsIdSequence.begin(); it != scenario.actionsIdSequence.end(); ++it)
        {
            this->actionsIdSequence.push_back(*it);
        }
        this->fitness = scenario.fitness;
    }
    void next_turn(State &currentState)
    {
        this->initState = &currentState;
        if (actionsIdSequence.size() > 0)
            actionsIdSequence.erase(actionsIdSequence.begin());
        update_action_sequence();
        initState->delete_old_actions();
    }
    void set_init_state(State &state)
    {
        this->initState = &state;
    }

    // simulation fcts
    void simulate(const int &nbOfTurnsInFuture)
    {
        init_simulation();
        // simulating the next nbOfTurnsInFuture turns
        while (!finalState.is_end() && simulate_turn() && finalState.get_turn() - initState->get_turn() < nbOfTurnsInFuture)
        {
        }
        evaluate_fitness();
    }

    void randomize() { this->actionsIdSequence.resize(0); } // To do : randomize more cleverly

    void display_answer()
    {
        Action *actionToDisplay = this->initState->get_action_corresponding_to_id(actionsIdSequence[0].actionId);
        if (actionsIdSequence.size() > 0)
        {
            std::cout << actionToDisplay->get_type();
            if (actionToDisplay->get_type() != "REST")
                std::cout << " " << actionToDisplay->get_id();

            if (actionToDisplay->get_type() == "CAST" && actionToDisplay->is_castable() && actionsIdSequence[0].timesUsed > 0)
                std::cout << " " << actionsIdSequence[0].timesUsed;

            std::cout << std::endl;
        }
        else
        {
            std::cerr << "ERROR scenario.display_answer(). No display possible" << std::endl;
            std::cout << "WAIT" << std::endl;
        }
    }

    friend std::ostream &operator<<(std::ostream &os, Scenario &scenario)
    {
        os << "displaying scenario :" << std::endl;
        os << "fitness : " << scenario.fitness << std::endl;
        os << "initState : " << *scenario.initState;
        os << "finalState : " << scenario.finalState;
        os << "action sequence size : " << scenario.actionsIdSequence.size() << std::endl;
        for (auto it = scenario.actionsIdSequence.begin(); it != scenario.actionsIdSequence.end(); ++it)
        {
            os << "actionId : " << (*it).actionId << std::endl;
        }
        return os;
    }
};

class Timer
{
private:
    std::chrono::time_point<std::chrono::high_resolution_clock> startTime;

public:
    Timer() {}
    ~Timer() {}

    void start() { this->startTime = std::chrono::high_resolution_clock::now(); }
    long long read() { return std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - this->startTime).count(); }
};

class IA
{
private:
    Timer timer;
    int scenarioNumber;
    Scenario tempScenario;
    std::vector<Scenario> scenarios;
    State currentState;
    int testFreeLearnId;
    int nbOfTurnsInFutureSimulated;

    // init turn functions
    void init_turn()
    {
        this->currentState.prepareForNextTurn();
        handle_inputs();
        prepare_scenarios();
        this->currentState.delete_old_actions();
    }
    void handle_inputs()
    {
        int actionCount; // the number of spells and recipes in play
        std::cin >> actionCount;
        std::cin.ignore();

        int actionId;           // the unique ID of this spell or recipe
        std::string actionType; // in the first league: BREW; later: CAST, OPPONENT_CAST, LEARN, BREW
        int delta0;             // tier-0 ingredient change
        int delta1;             // tier-1 ingredient change
        int delta2;             // tier-2 ingredient change
        int delta3;             // tier-3 ingredient change
        int price;              // the price in rupees if this is a potion
        int tomeIndex;          // in the first two leagues: always 0; later: the index in the tome if this is a tome spell, equal to the read-ahead tax
        int taxCount;           // in the first two leagues: always 0; later: the amount of taxed tier-0 ingredients you gain from learning this spell
        bool castable;          // in the first league: always 0; later: 1 if this is a castable player spell
        bool repeatable;        // for the first two leagues: always 0; later: 1 if this is a repeatable player spell
        for (int i = 0; i < actionCount; i++)
        {
            std::cin >> actionId >> actionType >> delta0 >> delta1 >> delta2 >> delta3 >> price >> tomeIndex >> taxCount >> castable >> repeatable;
            std::cin.ignore();
            if (actionType == "LEARN" && tomeIndex == 0)
            {
                testFreeLearnId = actionId;
            }
            this->currentState.addAction(actionId, actionType, delta0, delta1, delta2, delta3, price, tomeIndex, taxCount, castable, repeatable);
        }

        int inv0; // tier-0 ingredients in inventory
        int inv1;
        int inv2;
        int inv3;
        int score; // amount of rupees
        for (int i = 0; i < 2; i++)
        {
            std::cin >> inv0 >> inv1 >> inv2 >> inv3 >> score;
            std::cin.ignore();
            if (i == 0)
            {
                this->currentState.set(inv0, inv1, inv2, inv3, score);
            }
        }
    }
    void prepare_scenarios()
    {
        for (auto it = this->scenarios.begin(); it != this->scenarios.end(); ++it)
        {
            it->next_turn(this->currentState);
            it->simulate(this->nbOfTurnsInFutureSimulated);
        }
    }

    // search of optimal solution functions
    void search()
    {
        load_random_scenario();
        randomize_loaded_scenario();
        simulate_loaded_scenario();
        put_loaded_scenario_in_scenarios();
    }
    void load_random_scenario()
    {
        this->tempScenario.set_init_state(currentState);
    }
    void randomize_loaded_scenario()
    {
        this->tempScenario.randomize();
    }
    void simulate_loaded_scenario() { this->tempScenario.simulate(this->nbOfTurnsInFutureSimulated); }
    void put_loaded_scenario_in_scenarios()
    {
        if (tempScenario.get_fitness() > scenarios[0].get_fitness())
        {
            std::swap(tempScenario, scenarios[0]);
        }
    }

    // End of turn functions
    void end_turn()
    {
        display_answer();
        this->currentState.add_turn();
    }
    void display_answer()
    {
        if (this->currentState.get_turn() < 12)
        {
            std::cout << "LEARN " << testFreeLearnId << std::endl;
        }
        else
        {
            //std::cerr << "scenario chosen : " << scenarios[0];
            scenarios[0].display_answer();
        }
    }

public:
    IA() : scenarioNumber(1), nbOfTurnsInFutureSimulated(8)
    {
        currentState.addRestAction();
        for (size_t i = 0; i < this->scenarioNumber; i++)
        {
            scenarios.push_back(Scenario());
        }
    }
    ~IA() {}

    void play()
    {
        while (!currentState.is_end())
        {
            init_turn();
            this->timer.start();
            int iterations = 0;
            while (timer.read() < (long long)45000)
            {
                iterations++;
                search();
            }
            std::cerr << "end of turn : " << timer.read() << "us. nb of it : " << iterations << std::endl;
            end_turn();
        }
    }
};

int main()
{
    IA ia;

    ia.play();
}